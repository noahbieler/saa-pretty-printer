libraryDependencies ++= Seq(
  "org.scala-lang.modules" %% "scala-xml" % "1.0.6",
  "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.4",
  "org.scala-lang.modules" %% "scala-swing" % "1.0.2",
  // Test
  "org.scalatest" %% "scalatest" % "3.0.0"  % "test"
)

lazy val root = (project in file("."))
  .settings(
    organization := "ch.swissquant", 
    name := "saaPrettyPrinter",
    version := "0.1.0",
    scalaVersion := "2.11.8",
    mainClass in (Compile, run) := Some("ch.swissquant.utils.Parser")
  )
