# SAA Pretty Printer

A pretty printer for SAAs as defined in the soap requests and returns.

## Installing prerequisits

For compiling, use `sbt`, the Scala build tool, which can be installed
according to the instructions
[here](http://www.scala-sbt.org/0.13/docs/Installing-sbt-on-Linux.html).

Also make sure to install Scala itself (best is version 2.11).

## Running

To try the example, run 

    sbt run

## Continuous Integration

Use the provided `Jenkinsfile` in a pipeline project to use CI.
