package ch.swissquant.utils.ast

import scala.xml.{Elem, NodeSeq}

private object AST {

  implicit def elem2Double(nodeSeq: NodeSeq): Double = {
    nodeSeq(0).text.toDouble
  }

  implicit def elem2Int(nodeSeq: NodeSeq): Int = {
    nodeSeq(0).text.toInt
  }
}

case class AANodeWeight(id: Int, minWeight: Double, maxWeight: Double, weight: Double) {
}

object AANodeWeight {
  import AST._

  val name = "aaNodeWeight"

  def apply(xmlElement: Elem): AANodeWeight = {
    val node = (xmlElement \ "@aaNode").text.toInt
    val weight = (xmlElement \ "weight")(0)
    AANodeWeight(
      node,
      weight \ "@min",
      weight \ "@max",
      weight \ "@weight"
    )
  }
}


sealed trait BookingState { val name: String }
case object Original extends BookingState { val name = "original" }
case object Candidate extends BookingState { val name = "candidate"  } // TODO correct?

object BookingState {
  def apply(state: String): Option[BookingState] = state match {
    case Original.name => Some(Original)
    case Candidate.name => Some(Candidate)
    case _ => None
  }
}

sealed trait RestrictionOverrideRight { val name: String }
case object Forbidden extends RestrictionOverrideRight { val name = "forbidden" }

object RestrictionOverrideRight {
  def apply (right: String) = right match {
    case Forbidden.name => Some(Forbidden)
    case _ => None
  }
}

case class PositionEvaluation(
    accrual: Double,
    bookingState: BookingState,
    marketValue: Double,
    position: Int,
    restrictionOverrideRight: RestrictionOverrideRight
)

object PositionEvaluation {
  import AST._

  val name = "positionEvaluation"

  def apply(xml: Elem): Option[PositionEvaluation] = {
    for (
      bookingState <- BookingState((xml \ "@bookingState").text);
      right <- RestrictionOverrideRight((xml \ "@restrictionOverrideRight").text)
    ) yield {
      new PositionEvaluation(
        xml \ "@accrual",
        bookingState,
        xml \ "@marketValue",
        xml \ "@position",
        right
      )
    }
  }
}

case class RecommendedAssets(asset: Int)

object RecommendedAssets {
  val name = "recommendedAssets"

  def apply(xml: Elem) = new RecommendedAssets(xml.text.toInt)
}

case class AssetIncrements(
    accrual: Double,
    asset: Int,
    marketValue: Double
)

object  AssetIncrements {
  import AST._

  val name = "assetIncrements"

  def apply(xml: Elem): AssetIncrements = {
    new AssetIncrements(
      xml \ "@accrual",
      xml \ "@asset",
      xml \ "@marketValue"
    )
  }
}

sealed trait Operation
case class PortfolioOptimizationOperation(
    id: Int,
    targetBookingState: BookingState,
    investmentAmount: Double,
    maximumTurnOver: Double,
    minimumPositionSize: Int,
    minimumTradeVolume: Double // TODO correct?
) extends Operation

object Operation {
  import AST._

  def apply(xml: Elem): Option[Operation] = {
    xml.attributes
      .filter(m => m.key == "type")
      .value.headOption
      .map(_.text) match {
      case Some("avqsq:PortfolioOptimizationOperation") =>
        val bks: NodeSeq = xml \ "@targetBookingState"
        BookingState(bks.text)
          .map(bookingState =>
            new PortfolioOptimizationOperation(
              xml \ "@id",
              bookingState,
              xml \ "@investmentAmount",
              xml \ "@maximumTurnOver",
              xml \ "@minimumPositionSize",
              xml \ "@minimumTradeVolume"
            )
          )
      case _ =>
        None
    }
  }
}
