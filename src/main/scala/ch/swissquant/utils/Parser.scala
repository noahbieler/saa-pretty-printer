package ch.swissquant.utils

import java.io.InputStream

import scala.xml.XML

object Parser {

  def parseFile(filename: String) = {
    XML.loadFile(filename)
  }

  def parseInputStream(is: InputStream) = {
    XML.load(is)
  }
}
