package ch.swissquant.utils

import java.io.InputStream

import org.scalatest.FunSuite

class ParserTest extends FunSuite {

  test("testParseFile") {
    val inputStream: InputStream = getClass.getResourceAsStream("/request.xml")
    assert(inputStream != null, "Input Stream is null!")
    val result = Parser.parseInputStream(inputStream)
    assert(result != null)
  }
}
