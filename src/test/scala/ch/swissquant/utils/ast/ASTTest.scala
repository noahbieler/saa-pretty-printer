package ch.swissquant.utils.ast

import javax.xml.stream.events.Namespace

import org.scalatest.FunSuite

import scala.xml.{Elem, XML}

class ASTTest extends FunSuite {

  test("AANodeWeight") {
    val xml: Elem =
      <aaNodeWeights aaNode="3250831">
        <weight max="1.3" min=".7901" weight="1"/>
      </aaNodeWeights>
    val nodeWeight: AANodeWeight = AANodeWeight(xml)
    assert(nodeWeight.id == 3250831)
    assert(nodeWeight.maxWeight == 1.3)
    assert(nodeWeight.minWeight == 0.7901)
    assert(nodeWeight.weight == 1.0)
  }

  test("PositionEvaluation") {
    val xml =
      <positionEvaluation
        accrual="0"
        bookingState="original"
        marketValue="44581.17"
        position="3252407"
        restrictionOverrideRight="forbidden"/>

    val positionEvaluation: Option[PositionEvaluation] = PositionEvaluation(xml)
    assert(positionEvaluation.isDefined)
    positionEvaluation.foreach { position =>
      assert(position.accrual == 0)
      assert(position.bookingState == Original)
      assert(position.marketValue == 44581.17)
      assert(position.position == 3252407)
      assert(position.restrictionOverrideRight == Forbidden)
    }
  }

  test("AssetIncrments") {
    val xml = <assetIncrements accrual="0" asset="3252050" marketValue=".00923524677"/>

    val increments: AssetIncrements = AssetIncrements(xml)

    assert(increments.accrual == 0)
    assert(increments.asset == 3252050)
    assert(increments.marketValue == 0.00923524677)

  }

  test("PortfolioOptimizationType") {
    // TODO make it into a real operation
    val xml: Elem = <operations
        id="3252408"
        avqxsi:type="avqsq:PortfolioOptimizationOperation"
        targetBookingState="original"
        investmentAmount="0"
        maximumTurnOver="1"
        minimumPositionSize="0"
        minimumTradeVolume="1"/>

    val operation: Option[Operation] = Operation(xml)
    assert(operation.isDefined)

    operation.foreach {
      case op: PortfolioOptimizationOperation =>
        assert(op.id == 3252408)
        assert(op.targetBookingState == Original)
        assert(op.investmentAmount == 0)
        assert(op.maximumTurnOver == 1)
        assert(op.minimumPositionSize == 0)
        assert(op.minimumTradeVolume == 1)
      case _ =>
        fail(s"Unexpectedd type for $operation")
    }
  }
}
